import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { incrementar, decrementar } from './contador/contador.actions';
import { Observable } from 'rxjs';
import { appState } from './app.reducers';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  contador$: Observable<number>;
  constructor(private store: Store<appState>) {
    this.contador$ = store.pipe(select('contador'));
  }

  incrementar() {
    this.store.dispatch(incrementar());

  }
  decrementar() {
    this.store.dispatch(decrementar());

  }
}
