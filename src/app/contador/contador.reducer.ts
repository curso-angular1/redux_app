import { createReducer, on } from '@ngrx/store';
import * as AccionesReducer from './contador.actions';


export const initialState = 0;

const _counterReducer = createReducer(initialState,
  on(AccionesReducer.incrementar, state => state + 1),
  on(AccionesReducer.decrementar, state => state - 1),
  on(AccionesReducer.multiplicar, (state, { numero }) => state * numero),
  on(AccionesReducer.dividir, (state, { numero }) => state / numero),
  on(AccionesReducer.reset, state => 0),
);

export function Contadoreducer(state, action) {
  return _counterReducer(state, action);
}
