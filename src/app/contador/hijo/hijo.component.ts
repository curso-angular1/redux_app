import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { appState } from '../../app.reducers';
import * as AccionesReducer from '../contador.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.scss']
})
export class HijoComponent implements OnInit {

  contador$: Observable<number>

  constructor(private store: Store<appState>) { }

  ngOnInit(): void {
    this.contador$ = this.store.pipe(select('contador'));
  }

  multiplicar() {
    this.store.dispatch(AccionesReducer.multiplicar({ numero: 2 }));
  }
  dividir() {
    this.store.dispatch(AccionesReducer.dividir({ numero: 2 }));

  }

}
