import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { appState } from '../../app.reducers';
import { reset } from '../contador.actions';
@Component({
  selector: 'app-nieto',
  templateUrl: './nieto.component.html',
  styleUrls: ['./nieto.component.scss']
})
export class NietoComponent implements OnInit {

  contador$: Observable<number>;

  constructor(private store: Store<appState>) {
    this.contador$ = this.store.pipe(select('contador'));
  }

  ngOnInit(): void {
  }

  reset() {
    this.store.dispatch(reset())
  }
}
